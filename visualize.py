# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
from pathlib import Path
import pandas as pd


def main():
    data_file = Path(__file__).parent / "data.csv"
    df = pd.read_csv(data_file)
    df.plot.scatter(x=0, y=1)
    plt.show()


if __name__ == "__main__":
    main()
