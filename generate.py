# -*- coding: utf-8 -*-

from pathlib import Path
import pandas as pd
import numpy as np
import random
import math


def generate():
    x = np.linspace(0, 10 * math.pi, 1000)
    y = np.array([math.cos(p) + random.normalvariate(0, 0.2) for p in x])
    # Limit and quantize ordinate
    y = np.digitize(np.clip(y, -1, 1), np.linspace(-1, 1, 5))
    return {'x': x, 'y': y}


def main():
    data_file = Path(__file__).parent / "data.csv"
    df = pd.DataFrame(generate())
    df.to_csv(data_file, header=False, index=False)


if __name__ == "__main__":
    main()
